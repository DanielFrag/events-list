package producer

// Producer interface to post messages on message services
type Producer interface {
	PostMessage(message []byte) error
	StartProducer(opt map[string]interface{}) error
	StopProducer()
}
