package repository

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/DanielFrag/events-list/infra"
	"go.mongodb.org/mongo-driver/bson"
)

type dbEvent struct {
	Name string
}

// EventsMongoStorage implements mongoDB as a repository for event names
type EventsMongoStorage struct{}

// AddEvent add event name to events collection
func (e *EventsMongoStorage) AddEvent(event string) error {
	eventsCollection := infra.GetDatabase("events").Collection("events")
	opCtx, opCancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer opCancel()
	docRepresentation := bson.M{
		"name": event,
	}
	var eventDB dbEvent
	findOneErr := eventsCollection.FindOne(opCtx, docRepresentation).Decode(&eventDB)
	if eventDB.Name != "" || findOneErr == nil {
		return nil
	}
	_, insertError := eventsCollection.InsertOne(opCtx, docRepresentation)
	if insertError != nil {
		return insertError
	}
	return nil
}

// FilterEventsByPrefix return only the saved events that match the prefix
func (e *EventsMongoStorage) FilterEventsByPrefix(prefix string) ([]string, error) {
	eventsCollection := infra.GetDatabase("events").Collection("events")
	opCtx, opCancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer opCancel()
	cursor, cursorError := eventsCollection.Find(opCtx, bson.D{{
		"name",
		bson.D{{
			"$regex",
			fmt.Sprintf("^%s.*", prefix),
		}},
	}})
	if cursorError != nil {
		return nil, cursorError
	}

	defer cursor.Close(opCtx)
	result := make([]string, 0, 1)
	for cursor.Next(opCtx) {
		var evt dbEvent
		decodeError := cursor.Decode(&evt)
		if decodeError == nil && evt.Name != "" {
			result = append(result, evt.Name)
		}
	}
	return result, nil
}
