package handler

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/context"
	"gitlab.com/DanielFrag/events-list/repository"
)

// GetEvents return a list of events that match the requested prefix
func GetEvents(w http.ResponseWriter, r *http.Request) {
	prefix := r.URL.Query().Get("prefix")
	contextEventsRepository := context.Get(r, "EventsRepository")
	eventsRepository, ok := contextEventsRepository.(repository.EventsRepository)
	if !ok {
		http.Error(w, "Internal error", http.StatusInternalServerError)
		log.Println("Context EventsRepository conversion fail")
		return
	}
	events, filterError := eventsRepository.FilterEventsByPrefix(prefix)
	if filterError != nil {
		http.Error(w, "Internal error", http.StatusInternalServerError)
		log.Println(fmt.Sprintf("GetEvents error: %s", filterError))
		return
	}
	if len(events) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	resBody, jsonError := json.Marshal(events)
	if jsonError != nil {
		http.Error(w, "Internal error to build the events list", http.StatusInternalServerError)
		log.Println(fmt.Sprintf("GetEvents error: %s", jsonError))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(resBody)
}
