package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/DanielFrag/events-list/handler"
	"gitlab.com/DanielFrag/events-list/middleware"
	"gitlab.com/DanielFrag/events-list/utils"
)

//Route store the route data
type Route struct {
	Method      string
	Pattern     string
	HandlerFunc utils.HandlerFuncInjector
}

var routes = []Route{
	Route{
		Method:  "GET",
		Pattern: "/api/events",
		HandlerFunc: utils.HandlerFuncInjector{
			Dependencies: []func(http.HandlerFunc) http.HandlerFunc{
				middleware.GetEventsMiddleware,
			},
			Handler: handler.GetEvents,
		},
	},
	Route{
		Method:  "POST",
		Pattern: "/api/events",
		HandlerFunc: utils.HandlerFuncInjector{
			Dependencies: []func(http.HandlerFunc) http.HandlerFunc{
				middleware.JSONContentTypeChecker,
				middleware.PostEventMiddleware,
			},
			Handler: handler.PostEvent,
		},
	},
}

//NewRouter return the application router with its handlers
func NewRouter() http.Handler {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		route.HandlerFunc.InjectDependencies()
		router.
			HandleFunc(route.Pattern, route.HandlerFunc.Handler).
			Methods(route.Method)
	}
	return middleware.CorsSetup(router)
}
