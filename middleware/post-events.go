package middleware

import (
	"net/http"
	"strings"

	"github.com/gorilla/context"
)

// PostEventMiddleware set the events repository interface on request context
func PostEventMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		context.Set(r, "Producer", &kafkaProducer)
		next(w, r)
	})
}

//JSONContentTypeChecker check the content-type header of request allowing only application/json
func JSONContentTypeChecker(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		contentType := r.Header.Get("content-type")
		if contentType == "" || !strings.Contains(contentType, "application/json") {
			http.Error(w, "Only json is supported", http.StatusUnsupportedMediaType)
			return
		}
		next(w, r)
		return
	})
}
