# events-list
## Sinopse
Web api preparada para gravar eventos e devolver uma lista (filtrada) dos nomes desses eventos.
## Rotas
- GET /api/events
  - Retorna um set com os nomes dos eventos cadastrados até o momento
  - Pode filtrar eventos baseado em um prefixo, basta enviar na query string o prefixo desejado
    - Exemplo
    ```
    http://<host>:<port>/api/events?prefix=fo
    // supondo que os eventos cadastrados sejam: "foo", "far", "bar" e "foz"
    // resposta: ["foo", "foz"]
    ```
- POST /api/events
  - Envia um evento para o repositório de eventos
  - Contrato de entrada (formato do body):
    ```json
    {
      "event": "buy",
      "timestamp": "2016-09-22T13:57:31.2311892-04:00"
    }
    ```
## Decisões de projeto
- Como repositório de eventos foi escolhido um tópico do Kafka. Esta decisão permite o desacoplamento entre os produtores e os consumidores de eventos.
- O repositório de nomes de eventos é um dos que consomem o tópico de eventos do Kafka. Apesar de produtores e consumidores de eventos estarem na mesma web api, eles estão desacoplados e posteriormente podem ser separados em dois projetos.
## Dependências
- librdkafka-dev
  - Necessário para compilar o driver `gopkg.in/confluentinc/confluent-kafka-go.v1/kafka`
- MongoDB
  - Usado para guardar a lista de nomes de eventos
- Kafka
  - Usado para enviar/obter um novo evento ocorrido
## Instruções para montagem do ambiente de desenvolvimento (usando o docker)
### librdkafka-dev
#### Adicionar o repositório da confluent
- Obter chave pública do repositório
```
$ wget -qO - https://packages.confluent.io/deb/5.3/archive.key | sudo apt-key add -
```
- Adicionar o repositório no arquivo sources.list
```
$ sudo add-apt-repository "deb [arch=amd64] https://packages.confluent.io/deb/5.3 stable main"
```
- Atualizar o apt-get
```
$ sudo apt-get update
```
#### Obter o pacote librdkafka-dev que contém a librdkafka
```
$ apt-get install librdkafka-dev
```
### MongoDB
Sugere-se usar o docker para rodar o mongoDB
```
$ docker run --name mongo-docker -d -p 27017:27017 mongo
```
### Kafka
Sugere-se usar o docker para rodar o Kafka, porém o Kafka tem como dependência o Zookeeper.
- Criar a rede do docker que será usada para comunicação com o kafka
```
$ docker network create --subnet 172.16.0.0/24 kafka-net
```
- Baixar a imagem do zookeeper (dependência necessária para gerenciamento dos brokers do kafka)
```
$ docker pull confluentinc/cp-zookeeper
```
- Iniciar o container do zookeeper pertencente a rede kafka
```
$ docker run -d --network kafka-net --hostname zookeeper --name zookeeper -p 2181:2181 -e ZOOKEEPER_CLIENT_PORT=2181 -e ZOOKEEPER_TICK_TIME=2000 confluentinc/cp-zookeeper
```
- Baixar a image do kafka
```
$ docker pull confluentinc/cp-kafka
```
- Inciar o container do kafka, como pertencente a rede do docker chamada "kafka", com as seguintes variáveis de ambiente:
  - KAFKA_ZOOKEEPER_CONNECT: endereço para conexão com o zookeeper
  - KAFKA_ADVERTISED_LISTENERS: informe de como clientes devem se conectar ao kafka a partir de outras redes
  - KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: quantas réplicas um tópico terá
```
$ docker run -d --network kafka-net --hostname kafka --name kafka -p 9092:9092 -e KAFKA_ZOOKEEPER_CONNECT=zookeeper:2181 -e KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://kafka:29092,PLAINTEXT_HOST://localhost:9092 -e KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR=1 confluentinc/cp-kafka
```
#### kafkacat (opcional)
Com o kafkacat é possível obter informações sobre o cluster do Kafka
- Baixar o kafkacat
```
$ apt-get update
$ apt-get install kafkacat
```
- Obter meta dados de um broker
```
$ kafkacat -b localhost:9092 -L
```
## Build
```
$ go build
```
## Variáveis de ambiente
- BROKER_LIST
  - Lista com os endereços (\<host\>:\<port\>) dos brokers do kafka separados por vírgulas
    - Exemplo
    ```
    localhost:9092,localhost:9093,localhost:9094
    ```
- TOPIC_NAME
  - Nome do tópico em que os eventos serão gravados e consumidos
- GROUP
  - Nome do grupo de consumidores do tópico no kafka
- MONGODB_URI
  - String de conexão com o banco
## Run
```
$ export MONGODB_URI=mongodb://localhost:27017 BROKER_LIST=localhost:9092 TOPIC_NAME=events-list GROUP=events-list-api && ./events-list
```