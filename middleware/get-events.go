package middleware

import (
	"net/http"

	"github.com/gorilla/context"
)

// GetEventsMiddleware set the events repository interface on request context
func GetEventsMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		context.Set(r, "EventsRepository", &eventsRepository)
		next(w, r)
	})
}
