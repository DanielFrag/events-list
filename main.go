package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/DanielFrag/events-list/handler"
	"gitlab.com/DanielFrag/events-list/infra"
	"gitlab.com/DanielFrag/events-list/middleware"
	"gitlab.com/DanielFrag/events-list/router"
)

func main() {
	defer mainRecover()
	// listen OS signals
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	// handle the port to listen
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	// start DB
	startDbError := infra.StartDB()
	if startDbError != nil {
		panic(startDbError)
	}
	// solve the handler dependencies
	middleware.ManageHandlerDependencies()
	// start to listen the events channel
	handler.HandleEventNames(middleware.GetMessageChannel(), middleware.GetRepository())
	// routes definitions
	r := router.NewRouter()
	// server configs
	srv := &http.Server{
		Addr:         "0.0.0.0:" + port,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r,
	}
	// listening port in go routine
	go func() {
		log.Println("Starting server")
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()
	// block waiting a OS signal
	<-c
	// start gracefully shutdown with 30s for timeout
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(30*time.Second))
	defer cancel()
	srv.Shutdown(ctx)
	clean()
	log.Println("shutting down")
	os.Exit(0)
}

func clean() {
	middleware.StopDependencies()
	infra.StopDB()
}

func mainRecover() {
	rec := recover()
	// exception case
	if rec != nil {
		log.Println(rec)
		clean()
		os.Exit(1)
	}
}
