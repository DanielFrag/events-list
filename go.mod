module gitlab.com/DanielFrag/events-list

go 1.14

require (
	github.com/confluentinc/confluent-kafka-go v1.4.2 // indirect
	github.com/gorilla/context v1.1.1
	github.com/gorilla/mux v1.7.4
	go.mongodb.org/mongo-driver v1.3.5
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.4.2
)
