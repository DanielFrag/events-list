package handler

import (
	"encoding/json"
	"fmt"
	"log"

	"gitlab.com/DanielFrag/events-list/model"
	"gitlab.com/DanielFrag/events-list/repository"
)

// HandleEventNames extract the event name and save on event repository
func HandleEventNames(msgChannel <-chan []byte, evtRepo repository.EventsRepository) {
	go func() {
		for {
			msg, ok := <-msgChannel
			if !ok {
				break
			}
			var evt model.Event
			json.Unmarshal(msg, &evt)
			addError := evtRepo.AddEvent(evt.Name)
			if addError != nil {
				log.Println(fmt.Sprintf("AddEvent error: %s", addError))
			} else {
				log.Println(fmt.Sprintf("Add event: %s", evt))
			}
		}
	}()
}
