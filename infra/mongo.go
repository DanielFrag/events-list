package infra

import (
	"context"
	"errors"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var client *mongo.Client

func StartDB() error {
	uri := os.Getenv("MONGODB_URI")
	if uri == "" {
		return errors.New("The mongo uri is undefined")
	}
	var connectError error
	opCtx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, connectError = mongo.Connect(opCtx, options.Client().ApplyURI(uri))
	if connectError != nil {
		return connectError
	}
	pingErr := client.Ping(opCtx, readpref.Primary())
	if pingErr != nil {
		return pingErr
	}
	timeoutError := opCtx.Err()
	if timeoutError != nil {
		return timeoutError
	}
	return nil
}

func StopDB() error {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	err := client.Disconnect(ctx)
	if err != nil {
		return err
	}
	return nil
}

func GetDatabase(dbName string) *mongo.Database {
	return client.Database(dbName)
}
