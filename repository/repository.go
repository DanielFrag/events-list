package repository

// EventsRepository interface to manage event names
type EventsRepository interface {
	AddEvent(event string) error
	FilterEventsByPrefix(prefix string) ([]string, error)
}
