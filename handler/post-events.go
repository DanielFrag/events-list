package handler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/context"
	"gitlab.com/DanielFrag/events-list/model"
	"gitlab.com/DanielFrag/events-list/producer"
)

// PostEvent record the event data on producer
func PostEvent(w http.ResponseWriter, r *http.Request) {
	contextProducer := context.Get(r, "Producer")
	eventsProducer, ok := contextProducer.(producer.Producer)
	if !ok {
		http.Error(w, "Internal error", http.StatusInternalServerError)
		log.Println("Context Producer conversion fail")
		return
	}
	body, bodyReadError := ioutil.ReadAll(r.Body)
	if bodyReadError != nil {
		http.Error(w, "Error reading body request", http.StatusInternalServerError)
		log.Println(fmt.Errorf("Error on post event: %v", bodyReadError))
		return
	}
	var evt model.Event
	jsonError := json.Unmarshal(body, &evt)
	if jsonError != nil {
		http.Error(w, "Error on json parsing error", http.StatusInternalServerError)
		log.Println(fmt.Errorf("Error on post event: %v", jsonError))
		return
	}
	if evt.Name == "" || evt.Timestamp.IsZero() {
		http.Error(w, "Error on validate json data", http.StatusBadRequest)
		return
	}
	eventsProducer.PostMessage(body)
	w.WriteHeader(202)
	w.Write([]byte(""))
}
