package middleware

import (
	"os"

	"gitlab.com/DanielFrag/events-list/consumer"
	"gitlab.com/DanielFrag/events-list/producer"
	"gitlab.com/DanielFrag/events-list/repository"
)

var eventChannel = make(chan []byte)
var eventsRepository = repository.EventsMongoStorage{}
var kafkaProducer = producer.KafkaProducer{}
var kafkaConsumer = consumer.KafkaConsumer{}

// ManageHandlerDependencies resolve the handler dependencies
func ManageHandlerDependencies() {
	kafkaBrokerList := os.Getenv("BROKER_LIST")
	options := make(map[string]interface{})
	options["bootstrap.servers"] = kafkaBrokerList
	kafkaProducer.StartProducer(options)
	kafkaConsumer.SetEventChannel(eventChannel)
	kafkaConsumer.StartConsumer()
}

// GetMessageChannel returns the channel that receives messages
func GetMessageChannel() chan []byte {
	return eventChannel
}

// GetRepository returns the interface for the events repository
func GetRepository() repository.EventsRepository {
	return &eventsRepository
}

// StopDependencies stop the producer
func StopDependencies() {
	kafkaProducer.StopProducer()
	kafkaConsumer.StopConsumer()
}
